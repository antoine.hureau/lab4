package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter {

    private Rectangle2D box;
    private GridDimension gd;
    private double margin;

    public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
        this.box = box;
        this.gd = gd;
        this.margin = margin;
    }

    public Rectangle2D getBoundsForCell(CellPosition cp) {
        double x = box.getX();
        double y = box.getY();
        double width = box.getWidth();
        double height = box.getHeight();
        double rows = gd.rows();
        double cols = gd.cols();
        double row = cp.row();
        double col = cp.col();

        double availableWidth = width - (margin * (cols + 1));
        double availableHeight = height - (margin * (rows + 1));

        double cellWidth = availableWidth / cols;
        double cellHeight = availableHeight / rows;

        double cellX = x + col * cellWidth + margin * (col + 1);
        double cellY = y + row * cellHeight + margin * (row + 1);

        return new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
    }
}
