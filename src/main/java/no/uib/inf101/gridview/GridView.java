package no.uib.inf101.gridview;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.List;
import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;

public class GridView extends JPanel {

    private static final double OUTERMARGIN = 30;
    private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

    private IColorGrid grid;

    public GridView(IColorGrid grid) {
        this.grid = grid;
        this.setPreferredSize(new Dimension(400, 300));
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        drawGrid(g2);
        CellPositionToPixelConverter cp = new CellPositionToPixelConverter(
                new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, getWidth() - 2 * OUTERMARGIN,
                        getHeight() - 2 * OUTERMARGIN),
                grid, OUTERMARGIN);
        drawCells(g2, grid, cp);
    }

    private void drawGrid(Graphics2D g2) {
        g2.setColor(MARGINCOLOR);
        g2.fill(new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, getWidth() - 2 * OUTERMARGIN,
                getHeight() - 2 * OUTERMARGIN));
    }

    private static void drawCells(Graphics2D g2, CellColorCollection cellColors, CellPositionToPixelConverter cp) {
        List<CellColor> cells = cellColors.getCells();
        for (CellColor cell : cells) {
            CellPosition cellPos = cell.cellPosition();
            Rectangle2D cellBounds = cp.getBoundsForCell(cellPos);
            Color color = cell.color();
            if (color == null) {
                color = Color.DARK_GRAY;
            }
            g2.setColor(color);
            g2.fill(cellBounds);
            g2.setColor(Color.BLACK);
            g2.draw(cellBounds);
        }
    }
}
