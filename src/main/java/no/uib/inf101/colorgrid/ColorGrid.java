package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid{
  
  int rows;
  int cols;
  Color[][] grid;
  List<CellColor> cellColor = new ArrayList<>();

  public ColorGrid(int rows, int cols) {
    this.rows = rows;
    this.cols = cols;
    this.grid = new Color[rows][cols];
  }

  @Override
  public int rows() {
    return this.rows;
  }

  @Override
  public int cols() {
    return this.cols;
  }

  @Override
  public Color get(CellPosition pos) {
    return grid[pos.row()][pos.col()];
  }

  @Override
  public List<CellColor> getCells() {
    cellColor.clear();
    for (int row = 0; row < this.rows;row++){
      for (int cols = 0; cols < this.cols;cols++){
        this.cellColor.add(new CellColor(new CellPosition(row, cols), this.grid[row][cols]));
      }
    }
    return this.cellColor;
  }

  @Override
  public void set(CellPosition pos, Color color) {
      grid[pos.row()][pos.col()] = color;
  }

}
